# HomeAssignment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## API service to fetch data

Fetching github users' public repos using `https://api.github.com/users/username/repos` request link with header `Accept: application/vnd.github.v3+json`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.



