import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest,  HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient, private router: Router) { }

  // implementing api interceptor for any http request
  // along with request, we add header here
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Accept: 'application/vnd.github.v3+json'
      }
    });
    return next.handle(request);
  }

  // api end point to search public users' repo
  searchrepo = function (params: any) {
    console.log(params);
    return this.http.get('https://api.github.com/users/' + params.username + '/repos');
  };
}
