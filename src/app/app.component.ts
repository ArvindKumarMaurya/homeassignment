import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'home-assignment';
  userSearchField: FormControl = new FormControl();
  filterField: FormControl = new FormControl();
  resultArray: any[];
  displayArray: any[];

  constructor(private apiservice: ApiService) {}

  ngOnInit() {
    this.filterField.valueChanges.subscribe(val => { // subscribing value for formcontrol
      if (this.displayArray.length > 0) {
        this.displayArray = this.resultArray.filter(e => e.name.toLowerCase().startsWith(val.toLowerCase()));
      }
    });
  }

  // calling api on hitting go button
  searchUserRepo() {
    this.apiservice.searchrepo({username: this.userSearchField.value}).subscribe(result => {
      if (result) {
        this.resultArray = result;
        this.displayArray = this.resultArray;
      }
    });
  }

  // clearing fields on hitting clear
  clearField(fromFilter) {
    if (fromFilter) {
      this.filterField.setValue('');
    } else {
      this.userSearchField.setValue('');
      this.displayArray = [];
    }
  }
}
